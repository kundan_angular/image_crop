import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-crop',
  templateUrl: './image-crop.component.html',
  styleUrls: ['./image-crop.component.css']
})
export class ImageCropComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  imageChangedEvent: any = '';
    croppedImage: any = '';

    aspection_ratio: number = 3/4;
  
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }
    imageLoaded() {
        console.log("Image Loaded");
    }
    cropperReady() {
        console.log("cropper ready");
    }
    loadImageFailed() {
        console.log("Failed");
    }

    get_crop_shape( value, custom ){
      if(custom =="custom"){
        this.aspection_ratio = parseFloat(value);
        // console.log(this.aspection_ratio);
      }
      else{
        this.aspection_ratio = value;
        // console.log(this.aspection_ratio);
      }     
    }


}
