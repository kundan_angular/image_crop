import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImoprtDocComponent } from './imoprt-doc.component';

describe('ImoprtDocComponent', () => {
  let component: ImoprtDocComponent;
  let fixture: ComponentFixture<ImoprtDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImoprtDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImoprtDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
