import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Routes, RouterModule } from '@angular/router';
import { ImageCropComponent } from './components/image-crop/image-crop.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImoprtDocComponent } from './components/imoprt-doc/imoprt-doc.component';

const appRoutes:Routes=[  
  { path:'', redirectTo: '/home', pathMatch: 'full' },
  { path:'home',component: ImageCropComponent } 
]

@NgModule({
  declarations: [
    AppComponent,
    ImageCropComponent,
    ImoprtDocComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    ImageCropperModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
